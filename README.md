# PAW Prices

Tracking the availability and prices of the apartments in the building I live in. Serves as a test bed for new technology, languages, etc.

## Nov 27th 2019

![Alt text](./architecture.png?raw=true 'Architecture Diagram')

- 'Database' in lower left is legacy non-aurora RDS instance to be cleaned up
- Query resolvers are all done using Resolver Mapping Templates (VTL) to RDS (uses AWS RDS data-api)
- To avoid needing to be in a VPC, Scrape uses knex-data-api-client and Objection
- Aurora and Bastion are in account default VPC
