require('dotenv').config()
const {logger, appenders, transformers} = require('lambda-logging');

class NoCarriageReturnLayout {
  format(level, message, event) {
    const time = transformers.timestampTransformer
      .getFromEvent(event)
      .toISOString();
    const requestId = transformers.requestTransformer.getFromEvent(event);
    return `${time}\t${requestId}\t${message}`;
  }
}
logger.appender = new appenders.ConsoleAppender(new NoCarriageReturnLayout());

const scrapeIt = require('scrape-it');
const knexDataApiClient = require('knex-data-api-client');
const knex = require('knex');
const {Model} = require('objection');
const {InfluxDB, Point} = require('@influxdata/influxdb-client')

const Rent = require('./models/rent');
const Audit = require('./models/audit');
const Unit = require('./models/unit');
const unit_seeds = require('./seeds/units');

const availableUrl = 'https://parkavewestpdx.securecafe.com/onlineleasing/park-avenue-west/availableunits.aspx';
const DECIMAL = 10;

const {DRYRUN, AWS_LAMBDA_FUNCTION_NAME, INFLUXDB_V2_BUCKET, INFLUXDB_V2_TOKEN, INFLUXDB_V2_ORG, INFLUXDB_V2_URL} = process.env;

const dryRun = DRYRUN || AWS_LAMBDA_FUNCTION_NAME === 'test';

const scrapeOptions = {
  rents: {
    listItem: '.AvailUnitRow',
    data: {
      UnitNumber: {
        selector: '[data-label="Apartment"]',
        convert: x => parseInt(x.replace('#', ''), DECIMAL),
      },
      min: {
        selector: '[data-label="Rent"]',
        convert: x => parseRent(x, 0),
      },
      max: {
        selector: '[data-label="Rent"]',
        convert: x => parseRent(x, 1),
      },
    },
  },
};

const {AWS_REGION, DB_NAME, DB_ARN, DB_SECRET_ARN} = process.env;
const db = knex({
  debug: false,
  client: knexDataApiClient.postgres,
  connection: {
    secretArn: DB_SECRET_ARN,
    resourceArn: DB_ARN,
    database: DB_NAME,
    region: AWS_REGION,
  },
});
Model.knex(db);

function parseRent(rentString, index) {
  // $2,288-$2,922
  const parts = rentString.split('-');
  if (parts.length === 0) {
    console.log('No rent found', rentString);
    return 0;
  }
  if (parts.length === 1 && index > 0) {
    console.log('Non-range rent found', parts);
  }
  let part = parts[index] || parts[0]; // Fall back to 0th value
  part = part.replace('$', '').replace(',', '');
  part = parseInt(part, DECIMAL);
  return part;
}

async function saveToDatabase(results) {
  console.log('saveToDatabase');
  const {rents} = results;

  if (dryRun) {
    try {
      await Promise.all(rents.map(async rent => await seedUnit(rent.UnitNumber)));
    } catch (e) {
      console.log('dry run seed unit exception', e)
    }
    return rents;
  }

  return await Promise.all(
    rents.map(async rent => {
      try {
        return await Rent.fromJson(rent)
          .$query()
          .insert();
      } catch (e) {
        return await seedUnit(rent.UnitNumber);
      }
    }),
  );
}

async function seedUnit(unitNumber) {
  const unit = unit_seeds.find(unit => unit.number === unitNumber);
  if (unit) {
    try {
      const result = await Unit.fromJson(unit)
        .$query()
        .insert();
      console.log(`Added ${unitNumber}`);
      return result;
    } catch (e) {
      // Already exists
    }
  } else {
    console.log(`Seed missing ${unitNumber}`);
    return unitNumber
  }
}

// Called by graphql, timer
exports.handler = async (event, context) => {
  // Log the event argument for debugging and for use in local development.
  // console.log(JSON.stringify(event, undefined, 2));
  const {time} = event;
  if (time) {
    return await scrapeRent(event);
  }
};

async function scrapeRent(date) {
  console.log('Scraping');

  try {
    const rawRents = await scrapeIt(availableUrl, scrapeOptions);
    await sendToInflux(rawRents);
    const results = await saveToDatabase(rawRents);
    const audit = {
      date,
      result: `${results.length} records`,
    };

    if (dryRun) {
      return audit;
    }

    return await Audit.fromJson(audit)
      .$query()
      .insert();
  } catch (err) {
    console.log('Caught err', err);
    if (dryRun) {
      return {date, err};
    }
    return await Audit.fromJson({date, result: err})
      .$query()
      .insert();
  }
}

Array.prototype.flat = function() { return this.reduce((acc, val) => acc.concat(val), []) }

async function sendToInflux(results) {
  const {rents} = results;
  const writeApi = new InfluxDB({url: INFLUXDB_V2_URL, token: INFLUXDB_V2_TOKEN})
    .getWriteApi(INFLUXDB_V2_ORG, INFLUXDB_V2_BUCKET, 'ns', {writeFailed: console.log})
  writeApi.useDefaultTags({complex: 'parkavewest'})

  const points = rents.map(rent => {
    //{ UnitNumber: 512, min: 1568, max: 1701 }
    const { UnitNumber, min, max } = rent

    const details = unit_seeds.find(unit => unit.number === UnitNumber);
    if (details) {
      const { sqft, floorplan, beds, baths, misc } = details;
      const study = misc === '+ study';
      const balcony = misc === '+ balcony';

      const pmin = new Point('rent')
        .tag('unitNumber', UnitNumber)
        .tag('sqft', sqft)
        .tag('floorplan', floorplan)
        .tag('beds', beds+'')
        .tag('baths', baths)
        .tag('study', study)
        .tag('balcony', balcony)
        .floatField('value', min); // Could be 'price'

      const pmax = new Point('rent')
        .tag('unitNumber', UnitNumber)
        .tag('sqft', sqft)
        .tag('floorplan', floorplan)
        .tag('beds', beds+'')
        .tag('baths', baths)
        .tag('study', study)
        .tag('balcony', balcony)
        .floatField('value', max);

      return [pmin, pmax];
    } else {
      const pmin = new Point('rent')
        .tag('unitNumber', UnitNumber)
        .floatField('value', min); // Could be 'price'

      const pmax = new Point('rent')
        .tag('unitNumber', UnitNumber)
        .floatField('value', max);

      return [pmin, pmax];
    }
  }).flat();

  if (dryRun) {
    points.forEach(point => console.log(point.toLineProtocol()))
  } else {
    await writeApi.writePoints(points)
    await writeApi.flush()
  }
}
