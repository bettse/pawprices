const {Model} = require('objection');

class Audit extends Model {
  static get tableName() {
    return 'Audits';
  }

  $beforeInsert() {
    const date = new Date();
    this.date = date.toISOString().split('T')[0];
    this.createdAt = date.toISOString();
    this.updatedAt = date.toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }
}

module.exports = Audit;
