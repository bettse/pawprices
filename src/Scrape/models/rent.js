const {Model} = require('objection');

class Rent extends Model {
  static get tableName() {
    return 'Rents';
  }
  static get relationMappings() {
    const Unit = require('./unit');
    return {
      unit: {
        relation: Model.BelongsToOneRelation,
        modelClass: Unit,
        join: {
          from: 'Rents.UnitNumber',
          to: 'Units.number',
        },
      },
    };
  }

  $beforeInsert() {
    const date = new Date();
    this.date = date.toISOString().split('T')[0];
    this.createdAt = date.toISOString();
    this.updatedAt = date.toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }
}

module.exports = Rent;
