const {Model} = require('objection');

class Unit extends Model {
  static get tableName() {
    return 'Units';
  }

  static get idColumn() {
    return 'number';
  }

  static get relationMappings() {
    const Rent = require('./rent');
    return {
      rents: {
        relation: Model.HasManyRelation,
        modelClass: Rent,
        join: {
          from: 'Units.number',
          to: 'Rents.UnitNumber',
        },
      },
    };
  }

  $beforeInsert() {
    const date = new Date();
    this.createdAt = date.toISOString();
    this.updatedAt = date.toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }

  static get total() {
    return 202;
  }
}

module.exports = Unit;

/*
number: {type: Sequelize.INTEGER, primaryKey: true},
sqft: Sequelize.INTEGER,
floorplan: Sequelize.STRING, // Letter indicating floorplan style
beds: Sequelize.INTEGER,
baths: Sequelize.INTEGER,
views: Sequelize.STRING, // One or more letters for directions of windows (N/S/E/W)
misc: {
  // Other info, like having a study/den
  type: Sequelize.STRING,
  defaultValue: '',
},

*/
